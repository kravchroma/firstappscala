package factory

/**
  * Created by ROM_PC on 03.02.2016.
  */
class ConcreteProduct2 extends AbstaractProduct{
  var name : String = this.getClass().getSimpleName();

  override def getNameProduct(): String = name;

  override def mainAction(): String = "Action2";
}
