package factory

/**
  * Created by ROM_PC on 03.02.2016.f(pr1 == null) {
      pr1 = new ConcreteProduct1;
    }
    return pr1;
  */
object MyFactory extends AbstractFacrory{
  private val pr1 = new ConcreteProduct1;

  override def createProduct1(): ConcreteProduct1 = pr1;

  override def createProduct2(): ConcreteProduct2 = new ConcreteProduct2
}

class MyFactory {
  val pr1 = MyFactory.createProduct1();
  val pr2 = MyFactory.createProduct2();
  println(s"MyFactory: ${pr1.getNameProduct()} + ${pr2.getNameProduct()}");
}


