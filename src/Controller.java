import com.work.file.WorkFile;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;

import java.util.Random;

/**
 * Created by ROM_PC on 31.01.2016.
 */
public class Controller {

    @FXML
    private Button bt;

    @FXML
    private TextArea label;

    @FXML
    public void initialize() {
        bt.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                WorkFile file = new WorkFile("test.txt");
                file.writeFile("Click!!!");
                label.setText(file.readFile());
            }
        });
    }
}
