package com.work.file

import java.io.FileWriter

import scala.io.Source

/**
  * Created by ROM_PC on 30.01.2016.
  */
class WorkFile(fileName: String) {

  private var cont = "empty";

  private def startConstructor(): Unit = {
    println("File Name:" + fileName);
  }

  def writeFile(fileContent: String): Unit = {
    val file = new FileWriter(fileName, true);
    file.write(fileContent);
    file.flush();
  }

  def readFile(): String = {
    val content = Source.fromFile(fileName).mkString;
    this.cont = content;
    return content;
  }

  def getContent(): String = {
    return cont;
  }

  startConstructor();
}
