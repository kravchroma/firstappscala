package collection

import scala.collection.mutable
import scala.collection.mutable.ListBuffer


/**
  * Created by ROM_PC on 30.01.2016.
  */
class WorkCollection {

  var list:ListBuffer[Int] = null;

  def this(_list: ListBuffer[Int]) {
    this();
    list = _list;
  }

  def run() : Unit = {
    list += 50;
    list.remove(0);
    list.remove(0);
    println("foreach:")
    list.foreach((i : Int) => print(s"${i*3}, "));
    println("\nfilter:")
    list.filter((i : Int) => i > 2).foreach((i : Int) => print(s"${i}, "));
    val (par, notpar) = list.partition(_ % 2 == 0);
    println("\npartition:")
    par.foreach(printFormat _);
    println();
    notpar.sortWith(_ > _).foreach(printFormat(_));
    println("\nfind:");
    println(list.find(_ == 6).get);
  }

  private def printFormat(x : Int) {
    print(s"${x}, ");
  }
}
