import java.sql.{DriverManager, Statement, Connection}
import javafx.fxml.FXMLLoader
import javafx.scene.{Scene, Parent}
import javafx.stage.Stage
import javarx.JavaRxExample

import collection.WorkCollection
import com.work.file.WorkFile
import db.SQLiteDb
import javafx.application.Application

import factory.{AbstaractProduct, MyFactory}


import scala.collection.mutable.ListBuffer

/**
  * Created by ROM_PC on 30.01.2016.
  */
object ScalaApp {
  def main(args: Array[String]) {
    //==============================================
    println("Start first app Scala...");
    val wfile = new WorkFile("test.txt");
    println(wfile.getContent());
    wfile.writeFile("Write file my first app Scala");
    println(wfile.readFile().toString);
    println(wfile.getContent().toString);
    //==============================================
    val list = ListBuffer(0,0,-7,1,1,2,3,5,1,3,6);
    val coll = new WorkCollection(list);
    coll.run();
    //==============================================
    val db = new SQLiteDb();
    //==============================================
    FxApp.main(args);
    val factory = MyFactory
    val pr1 = factory.createProduct1();
    println(s"${pr1.getNameProduct()} - ${pr1.mainAction()}")
    val pr2 = factory.createProduct2();
    println(s"${pr2.getNameProduct()} - ${pr2.mainAction()}")
    pr1.name = "clear1";
    pr2.name = "clear2";

    val prNew1 = factory.createProduct1();
    println(s"${prNew1.getNameProduct()} - ${prNew1.mainAction()}")
    val prNew2 = factory.createProduct2();
    println(s"${prNew2.getNameProduct()} - ${prNew2.mainAction()}")

    new MyFactory();

    //==============================================
    val jrx = new JavaRxExample();
    jrx.run();
    jrx.testAsync();
    jrx.testAsync();
    jrx.testAsync();
    jrx.testSync();

    System.in.read();
  }
}
