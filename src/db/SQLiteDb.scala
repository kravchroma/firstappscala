package db

import java.sql.{DriverManager, Statement, Connection}

/**
  * Created by ROM_PC on 31.01.2016.
  */
class SQLiteDb {
  var c: Connection = null;
  var stmt: Statement = null;
  try {
    Class.forName("org.sqlite.JDBC");
    c = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\ROM_PC\\Documents\\kiosk_db\\NativeMagazines.sqlite");
    System.out.println("Opened database successfully");
    stmt = c.createStatement();
    val sql = "select * from USA;";
    val rs = stmt.executeQuery(sql);
    while (rs.next()) {
      println(s"${rs.getString(1)}|${rs.getString(1)} ");
    }
    stmt.close();
    c.close();
  } catch  {
    case e : Exception => println("er!")
  }
}
