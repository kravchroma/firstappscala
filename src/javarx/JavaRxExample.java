package javarx;


import com.work.file.WorkFile;
import factory.AbstaractProduct;
import factory.MyFactory;
import factory.MyFactory$;
import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.observables.MathObservable;
import rx.schedulers.Schedulers;
import scala.Product;

import java.io.IOException;
import java.io.SyncFailedException;
import java.util.*;

/**
 * Created by ROM_PC on 05.02.2016.
 */
public class JavaRxExample {

    Subscription subscriber;

    public void run() {
        Observable.from(new String[]{"roma", "dsfdf", "qweqwe"})
                .map(new Func1<String, String>() {
                    @Override
                    public String call(String s) {
                        return String.format("%s + %s", s, "map");
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        System.out.println(s);
                    }
                });

        Observable.from(new Integer[]{5, 8, 11, 1})
                .subscribeOn(Schedulers.newThread())
                .toSortedList(new Func2<Integer, Integer, Integer>() {
                    @Override
                    public Integer call(Integer integer, Integer integer2) {
                        return integer > integer2 ? -1 : 1;
                    }
                })
                .subscribe(new Action1<List<Integer>>() {
                    @Override
                    public void call(List<Integer> integers) {
                        Observable.from(integers).forEach(new Action1<Integer>() {
                            @Override
                            public void call(Integer integer) {
                                System.out.println(integer);
                            }
                        });
                        MathObservable.sumInteger(Observable.from(integers.toArray())
                                .flatMap(new Func1<Object, Observable<Integer>>() {
                                    @Override
                                    public Observable<Integer> call(Object o) {
                                        return Observable.just((Integer) o);
                                    }
                                }))
                                .subscribe(new Action1<Integer>() {
                                    @Override
                                    public void call(Integer integer) {
                                        System.out.println(integer);
                                    }
                                });
                    }
                });

        Observable.merge(
                Observable.just(MyFactory$.MODULE$.createProduct1()),
                Observable.just(MyFactory$.MODULE$.createProduct2()))
                .subscribe(new Action1<AbstaractProduct>() {
                    @Override
                    public void call(AbstaractProduct abstaractProduct) {
                        System.out.println(String.format("Observable -> name: %s, action: %s",
                                abstaractProduct.getNameProduct(),
                                abstaractProduct.mainAction()));
                    }
                });
        Observable.just(new WorkFile("test.txt"))
                .subscribeOn(Schedulers.newThread())
                .map(new Func1<WorkFile, WorkFile>() {
                    @Override
                    public WorkFile call(WorkFile workFile) {
                        workFile.writeFile("Observable");
                        return workFile;
                    }
                })
                .map(new Func1<WorkFile, String>() {
                    @Override
                    public String call(WorkFile workFile) {
                        return workFile.readFile();
                    }
                })
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String text) {
                        System.out.println(text);
                    }
                });

    }

    public void testAsync() {
        if (subscriber != null)
            subscriber.unsubscribe();

        List<String> list = new ArrayList<>();
        int count = 1000;
        Integer[] intList = new Integer[count];
        for (int i = 0; i < count; i++) {
            list.add(String.format("<<%2.10f>>", Math.abs(new Random().nextGaussian())));
            intList[i] = i;
        }

        Observable<String> s1 = Observable.from(list);
        Observable<Integer> i1 = Observable.from(intList);

        subscriber = Observable.zip(s1, i1, new Func2<String, Integer, Object[]>() {
            @Override
            public Object[] call(String s, Integer integer) {
                Object[] objects = new Object[]{s, integer};
                return objects;
            }
        }).filter(new Func1<Object[], Boolean>() {
            @Override
            public Boolean call(Object[] objects) {
                return (Integer) objects[1] > 10;
            }
        }).observeOn(Schedulers.newThread())
                .subscribe(new Subscriber<Object[]>() {
                    @Override
                    public void onCompleted() {
                        System.out.println("Completed");
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        System.out.println(throwable.getMessage());
                    }

                    @Override
                    public void onNext(Object[] o) {
                        System.out.println(o[0] + " " + o[1]);
                    }
                });
    }

    public void testSync() {
        Observable<String> ob1 = Observable.just(1).map(new Func1<Integer, String>() {
            @Override
            public String call(Integer integer) {
                return String.format("INT: %s", integer);
            }
        });

        Observable<String> ob2 = Observable.just(2).map(new Func1<Integer, String>() {
            @Override
            public String call(Integer integer) {
                return String.format("INT: %s", integer);
            }
        });

        Observable<String> ob3 = Observable.just(3).map(new Func1<Integer, String>() {
            @Override
            public String call(Integer integer) {
                return String.format("INT: %s", integer);
            }
        });

        Observable.merge(ob2, ob3)
                .concatWith(ob1)
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        System.out.println("Observable.merge: " + s);
                    }
                });

        ob1.mergeWith(ob2)
                .concatWith(ob3)
                .subscribe(new Action1<String>() {
            @Override
            public void call(String s) {
                System.out.println("mergeWith: " + s);
            }
        });
    }
}
